package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;
 
public class IncrementTest {
    @Test
    public void testgetCounter() throws Exception {
 
        int digit = new Increment().getCounter();
        assertEquals("Error", 1, digit);
       
    }
   @Test
    public void testdecreasecounter() throws Exception {
 
        int digits= new Increment().decreasecounter(0);
        assertEquals("Error", 1, digits);
 
    }
    @Test
    public void testdecreasecounter1() throws Exception {
 
        int digits= new Increment().decreasecounter(1);
        assertEquals("Error", 1, digits);
 
    }
    @Test
    public void testdecreasecounter2() throws Exception {
 
        int digits= new Increment().decreasecounter(2);
        assertEquals("Error", 1, digits);
 
    }
}
 